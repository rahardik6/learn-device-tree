# Compiler
CC = gcc
CFLAGS = -g3

# Include directory for libfdt
INCLUDES = -I./libfdt

# Source files for libfdt and the main program
LIBFDT_SRCS = $(wildcard libfdt/*.c)
LIBFDT_OBJS = $(LIBFDT_SRCS:.c=.o)
MAIN_SRC = test.c
MAIN_OBJ = $(MAIN_SRC:.c=.o)

# Executable
EXEC = test

all: $(EXEC)

$(EXEC): $(MAIN_OBJ) $(LIBFDT_OBJS)
	$(CC) $^ -o $@

# General rule for compiling source files to object files
%.o: %.c
	$(CC) $(CFLAGS) -c $< -o $@ $(INCLUDES)

clean:
	rm -f $(MAIN_OBJ) $(LIBFDT_OBJS) $(EXEC)

