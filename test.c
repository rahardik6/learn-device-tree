#include <stdio.h>
#include <stdlib.h>
#include <libfdt.h>
#include <fcntl.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>

void print_property(const void *fdt, int nodeoffset, int depth) {
    int len;
    const struct fdt_property *prop;
    int propoffset = fdt_first_property_offset(fdt, nodeoffset);

    // Print properties with indentation based on depth
    while (propoffset > 0) {
        prop = fdt_get_property_by_offset(fdt, propoffset, &len);
        if (prop) {
            for (int i = 0; i < depth; i++) printf("  ");
            printf("Property: %s\n", fdt_string(fdt, fdt32_to_cpu(prop->nameoff)));
        }
        propoffset = fdt_next_property_offset(fdt, propoffset);
    }
}

void traverse_nodes(const void *fdt, int nodeoffset, int depth) {
    int childoffset;
    const char *nodename;

    // Print current node name with indentation
    nodename = fdt_get_name(fdt, nodeoffset, NULL);
    for (int i = 0; i < depth; i++) printf("  ");
    printf("Node: %s\n", nodename);

    // Print properties of the current node
    print_property(fdt, nodeoffset, depth);

    // Recursively traverse child nodes
    childoffset = fdt_first_subnode(fdt, nodeoffset);
    while (childoffset >= 0) {
        traverse_nodes(fdt, childoffset, depth + 1);
        childoffset = fdt_next_subnode(fdt, childoffset);
    }
}

int main() {
    int fd;
    struct stat sb;
    void *fdt;

    fd = open("example.dtb", O_RDONLY);
    if (fd < 0) {
        perror("Error opening DTB file");
        return -1;
    }

    if (fstat(fd, &sb) < 0) {
        perror("Error getting file stats");
        close(fd);
        return -1;
    }

    fdt = malloc(sb.st_size);
    if (!fdt) {
        perror("Error allocating memory");
        close(fd);
        return -1;
    }

    if (read(fd, fdt, sb.st_size) != sb.st_size) {
        perror("Error reading DTB file");
        free(fdt);
        close(fd);
        return -1;
    }

    close(fd);

    if (fdt_check_header(fdt) != 0) {
        fprintf(stderr, "Error: Bad device tree header\n");
        free(fdt);
        return -1;
    }

    // Start traversal from the root node
    int rootoffset = fdt_path_offset(fdt, "/");
    if (rootoffset < 0) {
        printf("Error finding node.\n");
        free(fdt);
        return -1;
    }

    traverse_nodes(fdt, rootoffset, 0);

    free(fdt);
    return 0;
}

